/**
 *
 * Copyright (C) 2023 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package edu.ncc.nest.nestapp.GuestVisit.Fragments;

import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import edu.ncc.nest.nestapp.GuestDatabaseRegistration.DatabaseClasses.GuestRegistrySource;
import edu.ncc.nest.nestapp.R;

public class ManualFragment extends Fragment {

    public static final String TAG = ManualFragment.class.getSimpleName();
    String barcode, name, phone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_guest_visit_manual_entry, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) { //Recovering after a screen rotation
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            ((EditText) view.findViewById(R.id.guest_visit_barcode_entry)).setText(savedInstanceState.getString("barE"));
            ((EditText) view.findViewById(R.id.guest_visit_msie_pt1)).setText(savedInstanceState.getString("msiE1"));
            ((EditText) view.findViewById(R.id.guest_visit_msie_pt2)).setText(savedInstanceState.getString("msiE2"));

        }

        Log.d(TAG, "In Manual Fragment");

        // Create an instance of the database helper
        GuestRegistrySource db = new GuestRegistrySource(requireContext());

        // Functioning barcode: GHI-9012
        /*"John Doe", "123-456-7890", "N00123456",
                    "01-01-9999", "123 Simple Ave", "Nothingtown", "1234", "NY",
                    "NY", null, null, null, null, null,
                    null, null, null, null, null, null,
                    null, null, null, null,null, null, "GHI-9012");
         */



        ( view.findViewById(R.id.manual_submit_btn)).setOnClickListener(view1 -> {

            //Saving the information in the EditText views
            barcode = String.valueOf(((EditText) view.findViewById(R.id.guest_visit_barcode_entry)).getText());
            Log.d(TAG, "Barcode : " + barcode);

            //Will most likely make field 1 the guest's name
            name = String.valueOf(((EditText) view.findViewById(R.id.guest_visit_msie_pt1)).getText());
            Log.d(TAG, "Name : " + name);

            //Up for debate
            phone = String.valueOf(((EditText) view.findViewById(R.id.guest_visit_msie_pt2)).getText());
            Log.d(TAG, "Phone : " + phone);

            Bundle entryResults = new Bundle();
            // this String determines whether the user is in the database or not. if this string is assigned null, the user is not in the database
            boolean registered = false;

            // if the user enters no input, a Toast is displayed to tell the user to enter sign in information
            if (barcode.equals("") && name.equals("") && phone.equals("")) {
                Toast.makeText(ManualFragment.this.getContext(), "Please enter data", Toast.LENGTH_SHORT).show();
                // if the user doesn't sign in using a barcode and leaves the id or name field empty, a Toast is displayed telling them to enter both name and id
            } else if (barcode.equals("") && (name.equals("") || phone.equals(""))) {
                Toast.makeText(ManualFragment.this.getContext(), "Please enter both your name and phone number", Toast.LENGTH_SHORT).show();
            } else {
                // barcode takes precedence when signing in
                if (!barcode.equals("")) {
                    String[] test = new String[2];
                    // put entered barcode into the bundle
                    entryResults.putString("barcode", barcode);
                    test = db.isRegistered(barcode);
                    if (test[0] != null || test[1] != null) {
                        registered = true;
                        entryResults.putString("name", test[0]);
                        entryResults.putString("phone", test[1]);
                    }
                    // calls isRegistered method to see if the bar code is registered in the database. whatever is returned is put into the bundle
                    // IS_REGISTERED = db.isRegistered(barcode);

                    // Log.d(TAG, "is the barcode in the database? " + IS_REGISTERED);
                    // user chooses to log in using name & phone number
                } else {
                    // put the entered name and phone into the bundle
                    entryResults.putString("name", name);
                    entryResults.putString("phone", phone);
                    // commented out because of a syntax error isRegistered returns a boolean
                    //String barcode2 = db.isRegistered(name, phone);
                    // set to null to make the code compile
                    String barcode2 = null;
                    if (barcode2 != null) {
                        registered = true;
                        entryResults.putString("barcode", barcode2);
                    }
                    //Log.d(TAG, "is the name and phone in the database? " + barcode);
                }
                // the result of put into the bundle
                entryResults.putBoolean("IS_REGISTERED", registered);
                // give ConfirmationFragment access to the bundle and navigate to that fragment
                getParentFragmentManager().setFragmentResult("SCAN_CONFIRMED", entryResults);
                NavHostFragment.findNavController(ManualFragment.this)
                        .navigate(R.id.action_GV_ManualFragment_to_ConfirmationFragment);
            }

            /* //If barcode is entered other information is irrelevant
            if (!barcode.equals("")) {

                //Packaging the barcode in a neat little bundle
                    entryResults.putString("BARCODE", barcode);
                    Log.d(TAG , "Barcode value: " + barcode);

                    final String GUEST_NAME = db.isRegistered(barcode);

                    if (GUEST_NAME != null)


                        // If the guest is registered, include the guest's name in the result
                        entryResults.putString("GUEST_NAME", GUEST_NAME);

                //Allowing the bundle to be accessed from other fragments
                //Using Scan_confirmed to stop conflicts
                getParentFragmentManager().setFragmentResult("SCAN_CONFIRMED", entryResults);


                NavHostFragment.findNavController(ManualFragment.this)
                        .navigate(R.id.action_GV_ManualFragment_to_ConfirmationFragment);


            }
            //TODO Create other sign in method
            else if(!name.equals("") && !nccId.equals("")) {

                final String BARCODE = db.isRegistered(name, nccId);

                //Packaging the barcode in a neat little bundle
                entryResults.putString("BARCODE", BARCODE);
                Log.d(TAG, "Barcode value: " + barcode);

                if (BARCODE != null) {


                    // If the guest is registered, include the guest's name in the result
                    entryResults.putString("GUEST_NAME", name);

                    //Allowing the bundle to be accessed from other fragments
                    //Using Scan_confirmed to stop conflicts
                    getParentFragmentManager().setFragmentResult("SCAN_CONFIRMED", entryResults);

                } else {

                    NavHostFragment.findNavController(ManualFragment.this)
                            .navigate(R.id.action_GV_ManualFragment_to_ConfirmationFragment);
                }

            }

                else
                Toast.makeText(ManualFragment.this.getContext(), "Please enter data", Toast.LENGTH_SHORT).show(); */


        });
    }
}
