package edu.ncc.nest.nestapp.CheckExpirationDate.DatabaseClasses;

/**
 * Interface used for creating a listener, so the NestDBDataSource won't try to create/open a
 * database until the version number of the remote database has been checked.
 */
public interface OnTaskCompleted {
    void OnTaskCompleted();
}
