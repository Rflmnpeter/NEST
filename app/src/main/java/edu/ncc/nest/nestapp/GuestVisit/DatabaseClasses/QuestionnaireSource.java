package edu.ncc.nest.nestapp.GuestVisit.DatabaseClasses;

/**
 * Copyright (C) 2020 The LibreFoodPantry Developers.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import androidx.annotation.NonNull;

import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;

/**
 * QuestionnaireSource: This class finds and inserts data from and to the
 * {@link QuestionnaireHelper} class / database.
 */
public class QuestionnaireSource {

    public static final String TAG = QuestionnaireSource.class.getSimpleName();



    private final QuestionnaireHelper QUESTIONNAIRE_HELPER;
    private SQLiteDatabase writableDatabase;
    private SQLiteDatabase readableDatabase;
    private String[] counterStr = {QuestionnaireHelper.ROW_ID, QuestionnaireHelper.GUEST_ID, QuestionnaireHelper.ADULT_COUNT,
    QuestionnaireHelper.SENIOR_COUNT, QuestionnaireHelper.CHILD_COUNT, QuestionnaireHelper.FIRST_VISIT,QuestionnaireHelper.DATE, QuestionnaireHelper.VISIT_COUNTER};



    /************ Constructor ************/

    public QuestionnaireSource(Context context) {
        QUESTIONNAIRE_HELPER = new QuestionnaireHelper(context);
    }

    /************ Custom Methods Start ************/

    /**
     * open --
     * Opens the database
     * @return Returns a reference to this QuestionnaireSource object
     */
    public QuestionnaireSource open() throws SQLException {

        writableDatabase = QUESTIONNAIRE_HELPER.getWritableDatabase();

        readableDatabase = QUESTIONNAIRE_HELPER.getReadableDatabase();

        return this;

    }

    /**
     * close --
     * Closes the database
     */
    public void close() {

        writableDatabase.close();

        readableDatabase.close();

    }

    public void clearData() {
        writableDatabase.delete(QuestionnaireHelper.TABLE_NAME, null, null);
        readableDatabase.delete(QuestionnaireHelper.TABLE_NAME, null, null);
    }

    /**
     * submitQuestionnaire --
     * Submits answers by guest (guestID) into the database
     * @param guestID The ID of the guest the submission belongs to
     * @return Whether or not there was an error submitting the questionnaire
     */
    public long submitQuestionnaire(@NonNull String guestID, @NonNull String adultCount, @NonNull String seniorCount,
                                    @NonNull String childCount, @NonNull String firstVisit, @NonNull String visitCounter,@NonNull String date) {

        ContentValues submissionValues = new ContentValues();

        //Setup the data and time for when the Questionnaire was submitted
        DateTimeFormatter frmt = DateTimeFormatter.ofPattern("MMddyyyy HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        String currentTime = frmt.format(now);
        //todo testing for wuery methods added a date parameter.
        // Put the guest's id into cValues
        submissionValues.put(QuestionnaireHelper.GUEST_ID, guestID);
        submissionValues.put(QuestionnaireHelper.ADULT_COUNT, adultCount);
        submissionValues.put(QuestionnaireHelper.SENIOR_COUNT, seniorCount);
        submissionValues.put(QuestionnaireHelper.CHILD_COUNT, childCount);
        submissionValues.put(QuestionnaireHelper.FIRST_VISIT, firstVisit);
        submissionValues.put(QuestionnaireHelper.DATE, date);
        submissionValues.put(QuestionnaireHelper.VISIT_COUNTER, visitCounter);

        // Insert the submission in the database and return the row id it was stored at
        return (writableDatabase.insert(QuestionnaireHelper.TABLE_NAME, null, submissionValues));

    }



    /**
     * findSubmissions --
     * Finds submissions by a guest ( guestID ) and adds
     * them to a list of submissions.
     * @param guestID The ID to search for
     * @return The list of submissions
     */
    /*
    public List<QuestionnaireSubmission> findSubmissions(@NonNull String guestID) {

        List<QuestionnaireSubmission> submissions = new ArrayList<>();

        // Get all the guest records from the table (TABLE_NAME) who's field name (GUEST_ID) matches the field value (?).
        String sqlQuery = "SELECT * FROM " + QuestionnaireHelper.TABLE_NAME +
                " WHERE " + QuestionnaireHelper.GUEST_ID + " = ?";

        // Run the SQL query from above and replace the '?' character with the respective argument stored in the String[] array
        Cursor cursor = readableDatabase.rawQuery(sqlQuery, new String[] {guestID});

        // Loop through each cursor, convert them to submissions, and add it to the list
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext())

            submissions.add(convertCursorToSubmission(cursor));

        // Make sure we close the cursor since we don't need it anymore
        cursor.close();

        return submissions;

    }

    */

    /*
    /**
     * printSubmissions --
     * Searches through the database for questionnaires
     * submitted by a guest ( guestID ). Prints each
     * questionnaire submitted by the guest.
     * @param guestID The ID of the guest we're getting the submissions of
     */
    public void printSubmissions(@NonNull String guestID) {

        Log.i(TAG, "Printing all submissions by guest " + guestID + " from " + QuestionnaireHelper.DATABASE_NAME + ":");

        // Find each questionnaire submitted by the guest and print it to the log

    }
    /**
     * convertCursorToSubmission --
     * Converts a Cursor object to a QuestionnaireSubmission object.
     * @param c The Cursor to convert
     * @return Returns the result of the conversion
     */
    private QuestionnaireSubmission convertCursorToSubmission(Cursor c) {
        QuestionnaireSubmission entry = new QuestionnaireSubmission(c.getLong(0),
                c.getString(1),
                c.getString(2),
                c.getString(3),
                c.getString(4),
                c.getString(5),
                c.getString(6),
                c.getString(7));

        return entry;
    }

    /**
     * Finds the number of times the guest has visited
     * @param barcode the guest to find
     * @return the number of times this guest has visited as a string
     */
    public String getVisitCount(String barcode){
        try {
            QuestionnaireSubmission entry;
            Cursor cursor = readableDatabase.query(QUESTIONNAIRE_HELPER.TABLE_NAME,
                    counterStr,
                    QUESTIONNAIRE_HELPER.GUEST_ID + " = '" + barcode + "' ",
                    null,
                    null,
                    null,
                    QUESTIONNAIRE_HELPER.VISIT_COUNTER + " DESC ");
            Log.d("**CURSOR CHECK**", cursor.toString());
            cursor.moveToFirst();
            entry = convertCursorToSubmission(cursor);
            Log.d("**CURSOR CHECK**", entry.VISIT_COUTNER);
            return entry.VISIT_COUTNER;
        } catch(CursorIndexOutOfBoundsException e) {
            return "0";
        }
    }

    ////////////// Methods for use with the User Reports //////////////
    /**
     * getTotalVisitors - Counts the total number of visitors in a given month

     * @param month - the month you wish to check
     * @param year - the year you wish to check
     * @return - The number of visitors in the given month
     */
    public int getTotalVisitors(int month, int year) {
        String queryArgument = QUESTIONNAIRE_HELPER.monthFormat.format(month) + "__" + year + "%";
        // Retrieve all table entries from given month
        Cursor c = readableDatabase.query(
                QUESTIONNAIRE_HELPER.TABLE_NAME,
                new String[]{QUESTIONNAIRE_HELPER.ROW_ID},
                QUESTIONNAIRE_HELPER.DATE + " LIKE ? ",
                new String[]{queryArgument},
                null,
                null,
                null
        );
        // Count the entries
        int rowCount = c.getCount();
        // Close the database link
        c.close();
        // return the number of entries
        return rowCount;
    }

    /**
     * getTotalServed - Counts the total number of people served in a given month based on the
     * visitor's family size
     * @param month - The month you wish to check
     * @param year - The year you wish to check
     * @return - The number of people served in the given month
     */
    public int getTotalServed(int month, int year) {
        int servedCount = 0;
        String queryArgument = QUESTIONNAIRE_HELPER.monthFormat.format(month) + "__" + year + "%";
        // Retrieve all table entries from given month
        Cursor c = readableDatabase.query(
                QUESTIONNAIRE_HELPER.TABLE_NAME,
                new String[]{QUESTIONNAIRE_HELPER.ADULT_COUNT, QUESTIONNAIRE_HELPER.SENIOR_COUNT,
                        QUESTIONNAIRE_HELPER.CHILD_COUNT},
                QUESTIONNAIRE_HELPER.DATE + " LIKE ? ",
                new String[]{queryArgument},
                null,
                null,
                null
        );
        // Count the entries
        while (c.moveToNext()) {
            servedCount += c.getInt(0) + c.getInt(1) + c.getInt(2);
        }
        // Close the database link
        c.close();
        // return the number of entries
        return servedCount;
    }

    /**
     * getTotalServedByAge - Counts the number of people served within a given age range and month
     * based on the visitors' family size
     * @param month - The month you wish to check
     * @param year - The year you wish to check
     * @param age - Use QuestionnaireHelper.AGE_RANGE enum for this value. Specifies the age range you
     *            wish to search within
     * @return - The number of people served in the given month and age range
     */
    public int getTotalServedByAge(int month, int year,int age) {
        int servedCount = 0;
        Cursor c;
        String ageRange;
        String queryArgument = QUESTIONNAIRE_HELPER.monthFormat.format(month) + "__" + year + "%";
        // Sets which column to pull up based on the given AGE_RANGE variable
        switch(age) {
            case 0:
               ageRange = QUESTIONNAIRE_HELPER.CHILD_COUNT;
                break;
            case 1:
                ageRange = QUESTIONNAIRE_HELPER.ADULT_COUNT;

                break;
            case 2:
                ageRange =QUESTIONNAIRE_HELPER.SENIOR_COUNT;
                break;
            default:
                ageRange = "";
        }
        // Pulls the necessary data from the database
        c = readableDatabase.query(
                QUESTIONNAIRE_HELPER.TABLE_NAME,
                new String[]{ageRange},
                QUESTIONNAIRE_HELPER.DATE + " LIKE ?",
                new String[]{queryArgument},
                null,
                null,
                null
        );
        // Count the entries
        while (c.moveToNext())
            servedCount += c.getInt(0);
        // Close the database link
        c.close();
        // return the number of entries
        return servedCount;
    }

    /**
     * getTotalPersonVisits - Counts the number of times a specific guest has visited in a given
     * month
     * @param month - The month you wish to check
     * @param year - the year you wish to check
     * @param guestId - The ID of the guest you wish to check
     * @return - the number of times a specific guest has visited in the given month
     */
    public int getTotalPersonVisits(int month, int year, String guestId) {
        int visitCount = 0;
        String queryArgument = QUESTIONNAIRE_HELPER.monthFormat.format(month) + "__" + year + "%";
        // Pulls the necessary data from the database
        Cursor c = readableDatabase.query(
                QUESTIONNAIRE_HELPER.TABLE_NAME,
                new String[]{QUESTIONNAIRE_HELPER.VISIT_COUNTER},
                QUESTIONNAIRE_HELPER.DATE + " LIKE ? AND " + QUESTIONNAIRE_HELPER.GUEST_ID + " = ?",
                new String[]{queryArgument, guestId},
                null,
                null,
                null
        );
        // Count the entries
        while (c.moveToNext())
            visitCount++;
        // Close the database link
        c.close();
        // return the number of entries
        return visitCount;
    }









}
